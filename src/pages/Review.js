import React, { useState, useEffect } from "react";
import axios from "axios";
import { Container, Input, Label, FormGroup, Button, Form } from "reactstrap";

const Review = () => {
  const [Rating, setRating] = useState([]);
  const [Review, setReview] = useState([]);
  const [reviewData, setReviewData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const handleChange1 = (e) => setRating(e.target.value);
  const handleChange2 = (e) => setReview(e.target.value);

  useEffect(() => {
    setIsLoading(true);
    getData();
    setIsLoading(false);
  }, []);

  const deleteHandler = (e, id) => {
    e.preventDefault();
    // console.log("test");
    setIsLoading(true);
    const url = "https://5fa4bcd2732de900162e85ef.mockapi.io/api/reviews";
    axios.delete(`${url}/${id}`).then((res) => {
      getData();
      setIsLoading(false);
    });
  };

  const submit = (e) => {
    e.preventDefault();
    setIsLoading(true);
    const user = {
      rating: Rating,
      review: Review,
    };
    axios
      .post("https://5fa4bcd2732de900162e85ef.mockapi.io/api/reviews", user)
      .then((res) => {
        console.log(res);
        console.log(res.data);
        getData();
        setIsLoading(false);
      })
      .catch((err) => console.log(err));
  };

  const getData = () => {
    axios
      .get("https://5fa4bcd2732de900162e85ef.mockapi.io/api/reviews")
      .then((res) => {
        console.log(res.data);
        setReviewData(res.data);
      });
  };

  return (
    <Container className="mt-5">
      <>
        <h1>Review</h1>
        <br />
      </>
      <Form onSubmit={submit}>
        <Label>Rating</Label>
        <Input
          type="select"
          name="select"
          id="exampleSelect"
          onChange={handleChange1}
        >
          <option selected="true" disabled="disabled">
            Select Rating
          </option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </Input>
        <FormGroup className="mt-3">
          <Label for="exampleText">Review</Label>
          <Input
            type="textarea"
            name="text"
            id="exampleText"
            onChange={handleChange2}
          />
        </FormGroup>
        <Button type="submit" className="mt-3" color="primary">
          Submit
        </Button>
      </Form>
      <>
        <br />
        <br />
        <br />
        {/* <p>{Rating}</p> */}
        {/* <p>{Review}</p> */}
        {isLoading && <p>Loading Data...</p>}
        {!isLoading &&
          reviewData.map((data) => (
            <div key={data.id}>
              Id: {data.id}
              <br />
              Rating: {data.rating} <br />
              Review: {data.review} <br />
              <button onClick={(e) => deleteHandler(e, data.id)}>Delete</button>
              <hr />
            </div>
          ))}
      </>
    </Container>
  );
};

export default Review;
