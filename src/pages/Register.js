// import React from "react";
import React, { useState } from "react";
// import RegisterComp from "./components/RegisterComp";
import axios from "axios";
import { Button, Container, Input, Label, FormGroup, Form } from "reactstrap";

const Register = () => {
  //   <RegisterComp />;
  const [firstName, setFirstName] = useState([]);
  const [lastName, setLastName] = useState([]);
  const [gender, setGenderName] = useState([]);
  const [email, setEmailName] = useState([]);
  const [password, setPasswordName] = useState([]);

  const handleChange1 = (e) => setFirstName(e.target.value);
  const handleChange2 = (e) => setLastName(e.target.value);
  const handleChange3 = (e) => setEmailName(e.target.value);
  const handleChange4 = (e) => setPasswordName(e.target.value);
  const handleChange5 = (e) => setGenderName(e.target.value);

  const submit = (e) => {
    e.preventDefault();
    const user = {
      firstName: firstName,
      lastName: lastName,
      gender: gender,
      email: email,
      password: password,
    };
    axios
      .post("https://5fa4bcd2732de900162e85ef.mockapi.io/api/register", user)
      .then((res) => {
        console.log(res);
        console.log(res.data);
      });
  };

  return (
    <Container className="mt-5">
      <>
        <h1>Register</h1>
        <br />
      </>
      <Form onSubmit={submit}>
        <Label>First Name</Label>
        <Input
          type="text"
          id="firstName"
          name="firstName"
          placeholder=""
          onChange={handleChange1}
        />
        <Label className="mt-3">Last Name</Label>
        <Input
          type="text"
          id="lastName"
          name="lastName"
          placeholder=""
          onChange={handleChange2}
        />
        <Label className="mt-3">Gender</Label>
        <FormGroup check>
          <Label check>
            <Input
              type="radio"
              name="gender"
              onChange={handleChange5}
              value="Laki-Laki"
            />{" "}
            Laki Laki
          </Label>
        </FormGroup>
        <FormGroup check>
          <Label check>
            <Input
              type="radio"
              name="gender"
              onChange={handleChange5}
              value="Perempuan"
            />
            Perempuan
          </Label>
        </FormGroup>
        <Label className="mt-3">Email</Label>
        <Input
          type="text"
          id="email"
          name="email"
          placeholder=""
          onChange={handleChange3}
        />
        <Label className="mt-3">Password</Label>
        <Input
          type="password"
          id="password"
          name="password"
          placeholder=""
          onChange={handleChange4}
        />
        <Button type="submit" className="mt-3" color="primary">
          Submit
        </Button>
      </Form>

      <>
        <br />
        <br />
        <br />
        <p>{firstName}</p>
        <p>{lastName}</p>
        <p>{gender}</p>
        <p>{email}</p>
        <p>{password}</p>
      </>
    </Container>
  );
};

export default Register;
