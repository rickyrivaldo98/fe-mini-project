import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import {
  Container,
  Row,
  Navbar,
  NavbarBrand,
  Nav,
  NavItem,
  NavbarToggler,
  Collapse,
  Input,
} from "reactstrap";

const TopMenu = () => {
  const [collapsed, setCollapsed] = useState(true);
  const toggleNavbar = () => setCollapsed(!collapsed);
  return (
    <>
      <Navbar color="faded" light expand="md">
        <Container>
          <NavbarBrand>
            <NavLink to="/" style={{ textDecoration: "none", color: "Black" }}>
              Home
            </NavLink>
          </NavbarBrand>
          <NavbarToggler onClick={toggleNavbar} className="mr-2" />
          <Collapse isOpen={!collapsed} navbar>
            <Nav className="ml-lg-5" navbar>
              <NavItem>
                <NavLink
                  to="/register"
                  activeClassName="active"
                  className="nav-link p-2"
                >
                  Register
                </NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Container>
      </Navbar>
    </>
  );
};

export default TopMenu;
