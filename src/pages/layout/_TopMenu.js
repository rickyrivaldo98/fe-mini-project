import React, { useState } from "react";
import {
  Container,
  Row,
  Navbar,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  NavbarToggler,
  Collapse,
  Input,
} from "reactstrap";

const TopMenu = () => {
  const [collapsed, setCollapsed] = useState(true);
  const toggleNavbar = () => setCollapsed(!collapsed);
  return (
    <>
      <Navbar color="faded" light expand="md">
        <Container>
          <NavbarBrand className="text-dark mr-lg-5">Testing</NavbarBrand>
          <NavbarToggler onClick={toggleNavbar} className="mr-2" />
          <Collapse isOpen={!collapsed} navbar>
            <Input
              className="mt-2"
              type=""
              id=""
              name=""
              placeholder="Sarch Movie"
            />
            <Nav className="ml-lg-5" navbar>
              <NavItem>
                <NavLink
                  to="/about"
                  activeClassName="active"
                  className="nav-link p-2"
                >
                  Sign&nbsp;In
                </NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Container>
      </Navbar>
    </>
  );
};

export default TopMenu;
