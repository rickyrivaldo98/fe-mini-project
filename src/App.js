import React from "react";
import { Switch, Route } from "react-router-dom";
import TopMenu from "./pages/layout/TopMenu";
import Register from "./pages/Register";
import Review from "./pages/Review";
import "./App.css";

const App = () => {
  return (
    <>
      <TopMenu />
      <Switch>
        <Route path="/register">
          <Register />
        </Route>
        <Route exact path="/">
          <Review />
        </Route>
      </Switch>
    </>
  );
};

export default App;
